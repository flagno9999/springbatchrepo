package com.spbatch.multiReader;

import com.spbatch.multiReader.com.spbatch.multiReader.model.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
import org.springframework.batch.item.file.FlatFileItemReader;
import org.springframework.batch.item.file.FlatFileItemWriter;
import org.springframework.batch.item.file.MultiResourceItemReader;
import org.springframework.batch.item.file.mapping.BeanWrapperFieldSetMapper;
import org.springframework.batch.item.file.mapping.DefaultLineMapper;
import org.springframework.batch.item.file.transform.BeanWrapperFieldExtractor;
import org.springframework.batch.item.file.transform.DelimitedLineAggregator;
import org.springframework.batch.item.file.transform.DelimitedLineTokenizer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;

@Configuration
@EnableBatchProcessing
public class batchConfiguration {

    private static final Logger log = LoggerFactory.getLogger(batchConfiguration.class);

    @Autowired
    public JobBuilderFactory jobBuilderFactory;

    @Autowired
    public StepBuilderFactory stepBuilderFactory;

    @Value("classpath:user-*.csv")
    private Resource[] resources;


    @Bean
    public FlatFileItemReader<User> reader() {
        FlatFileItemReader<User> reader = new FlatFileItemReader<User>();
        reader.setLineMapper(new DefaultLineMapper<User>() {{
            setLineTokenizer(new DelimitedLineTokenizer() {{
                setNames(new String[]{"name", "gender"});
            }});
            setFieldSetMapper(new BeanWrapperFieldSetMapper<User>() {{
                setTargetType(User.class);
            }});
        }});return reader;
    }
    @Bean
    public MultiResourceItemReader<User> multiResourceItemReader(){
        MultiResourceItemReader<User> multi = new MultiResourceItemReader<User>();
        multi.setResources(resources);
        multi.setDelegate(reader());
        return multi;
    }

    @Bean
    public FlatFileItemWriter<User> writer(){
        log.info("wrinting...");
        FlatFileItemWriter<User> writer = new FlatFileItemWriter<User>();
        writer.setResource(new ClassPathResource("out/users.csv"));
        writer.setAppendAllowed(true);
        writer.setLineAggregator(new DelimitedLineAggregator<User>(){{
            setDelimiter(",");
            setFieldExtractor(new BeanWrapperFieldExtractor<User>(){{
                setNames(new String[]{"id","name"});
            }});
        }});
    log.info("Returnig writer....");
        return writer;
    }

    @Bean
    public Step step1(){
        return stepBuilderFactory.get("step1")
                .<User,User>chunk(10)
                .reader(reader())
                .writer(writer())
                .build();
    }
    @Bean
    public Job mainJob(){
        return jobBuilderFactory.get("mainJob")
                .incrementer(new RunIdIncrementer())
                .flow(step1())
                .end()
                .build();
    }

}
